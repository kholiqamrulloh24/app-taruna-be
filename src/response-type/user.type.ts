interface UserType {
  id: number;
  name: string;
  username: string;
  accessToken: string;
}

export default UserType;
