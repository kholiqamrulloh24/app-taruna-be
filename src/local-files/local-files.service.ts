import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import LocalFile from './entities/local-file.entity';

@Injectable()
export class LocalFilesService {
  constructor(
    @InjectRepository(LocalFile)
    private localFileRepository: Repository<LocalFile>
  ){}
  
  async saveLocalFileData(fileData?: LocalFileDto) {
    const newFile = await this.localFileRepository.create(fileData);
    await this.localFileRepository.save(newFile);
    return newFile;
  }

  async getFileById(fileId: number) {
    const file = await this.localFileRepository.findOneBy({ id: fileId });
    if(!file) {
      throw new NotFoundException();
    }
    return file;
  }
}
