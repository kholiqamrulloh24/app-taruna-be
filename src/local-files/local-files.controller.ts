import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe, Res, StreamableFile, ClassSerializerInterceptor, UseInterceptors } from '@nestjs/common';
import { createReadStream } from 'fs';
import { LocalFilesService } from './local-files.service';
import { Response } from 'express';
import { join } from 'path';
import { warn } from 'console';

@Controller('local-files')
@UseInterceptors(ClassSerializerInterceptor)
export class LocalFilesController {
  constructor(private readonly localFilesService: LocalFilesService) {}

  @Get(':id')
  async getDatabaseFileById(@Param('id', ParseIntPipe) id:number, @Res({ passthrough: true }) response: Response) {
    const file = await this.localFilesService.getFileById(id);

    const stream = createReadStream(join(process.cwd(), file.path));

    response.set({
      'Content-Disposition': `inline; filename"${file.filename}"`,
      'Content-Type': file.mimetype
    })

    return new StreamableFile(stream);
  }

  @Get('/detail/:id')
  async getDetailFileById(@Param('id', ParseIntPipe) id:number, @Res({ passthrough: true }) response: Response) {
    const file = await this.localFilesService.getFileById(id);

    return file;
  }
}
