import { Test, TestingModule } from '@nestjs/testing';
import { SimulasiService } from './simulasi.service';

describe('SimulasiService', () => {
  let service: SimulasiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SimulasiService],
    }).compile();

    service = module.get<SimulasiService>(SimulasiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
