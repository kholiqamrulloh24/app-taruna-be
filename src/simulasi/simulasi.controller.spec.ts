import { Test, TestingModule } from '@nestjs/testing';
import { SimulasiController } from './simulasi.controller';
import { SimulasiService } from './simulasi.service';

describe('SimulasiController', () => {
  let controller: SimulasiController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SimulasiController],
      providers: [SimulasiService],
    }).compile();

    controller = module.get<SimulasiController>(SimulasiController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
