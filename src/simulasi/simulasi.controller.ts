import { Controller, Query, Req, Get, Post, Body, Patch, Param, Delete, UseGuards, UploadedFile, UseInterceptors, BadRequestException } from '@nestjs/common';
import { SimulasiService } from './simulasi.service';
import { CreateSimulasiDto } from './dto/create-simulasi.dto';
import { UpdateSimulasiDto } from './dto/update-simulasi.dto';
import { ApiConsumes, ApiCreatedResponse, ApiOkResponse, ApiTags, ApiBody } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth';
import LocalFilesInterceptor from 'src/local-files/local-files.interceptor';
import {Express } from 'express';
import { PaginationParams } from 'src/utils/types/paginationParams';

@Controller('simulations')
@ApiTags('simulations')
export class SimulasiController {
  constructor(private readonly simulasiService: SimulasiService) {}
  
  @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(LocalFilesInterceptor({
    fieldName: 'file',
    path: '/simulations',
    fileFilter: (request, file, callback) => {
      if (!file.mimetype.includes('image')) {
         return callback(new BadRequestException('Provide a valid image'), false);
      }
      callback(null, true);
    },
    limits: {
      fileSize: Math.pow(1024, 2)
    }
  }))
  @ApiConsumes('multipart/form-data')
  create(@Req() request, @Body() createSimulasiDto: CreateSimulasiDto, @UploadedFile() file?: Express.Multer.File) {
    const userId = request.user.id;

    return this.simulasiService.create(createSimulasiDto, userId, file);
  }
  
  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@Req() request, @Query() query: { status : boolean }, 
    @Query() { offset, limit, startId } : PaginationParams
  ) {
    const userId = request.user.id;

    return this.simulasiService.findAll(userId, query.status , offset, limit, startId);
  }

  @Get('/status')
  status() {
    return this.simulasiService.getStatus();
  }
  
  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.simulasiService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSimulasiDto: Partial<UpdateSimulasiDto>) {
    return this.simulasiService.update(+id, updateSimulasiDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':idfalse')
  remove(@Param('id') id: string) {
    return this.simulasiService.remove(+id);
  }

}
