import { Module } from '@nestjs/common';
import { SimulasiService } from './simulasi.service';
import { SimulasiController } from './simulasi.controller';
import { ConfigModule } from '@nestjs/config';
import { User, Role } from 'src/auth/entities';
import { UnitKerja } from 'src/karyawan/entities';
import { LocalFilesModule } from 'src/local-files/local-files.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Simulasi } from './entities/simulasi.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Role, UnitKerja, Simulasi]), LocalFilesModule, ConfigModule],
  controllers: [SimulasiController],
  providers: [SimulasiService]
})
export class SimulasiModule {}
