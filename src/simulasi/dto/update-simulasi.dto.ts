import { PartialType } from '@nestjs/swagger';
import { CreateSimulasiDto } from './create-simulasi.dto';
import { ApiProperty } from '@nestjs/swagger';
import { 
    IsBoolean,
    IsNotEmpty,
    IsNumber,
    IsString,
} from "class-validator";
import { Type } from "class-transformer";

export class UpdateSimulasiDto {
    @IsString()
    @ApiProperty()
    environment_condition: string;

    @IsString()
    @ApiProperty()
    weather_condition: string;

    @IsString()
    @ApiProperty()
    cadets_condition: string;

    @IsString()
    @ApiProperty()
    full_report: string;

    @IsString()
    @ApiProperty()
    location: string;
 
    @ApiProperty()
    status: boolean;
}
