import { ApiProperty } from "@nestjs/swagger";
import { 
    IsBoolean,
    IsDate,
    IsNotEmpty,
    IsNumber,
    IsString,
} from "class-validator";
import { JoinColumn, ManyToOne } from "typeorm";
import { User } from "src/auth/entities";
import { Type, Transform } from "class-transformer";

export class CreateSimulasiDto {
    
    @Type(() => Number)
    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    respondent: number;
    
    //@Type(() => Date)
    @IsNotEmpty()
    @ApiProperty()
    report_date: string;


    //@Type(() => Date)
    @IsNotEmpty()
    @ApiProperty()
    report_time: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    environment_condition: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    weather_condition: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    cadets_condition: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    full_report: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    location: string;
    
    // @Type(() => Number)
    // @IsNumber()
    // @IsNotEmpty()
    // @ApiProperty()
    // latitude: number;
    //
    // @Type(() => Number)
    // @IsNumber()
    // @IsNotEmpty()
    // @ApiProperty()
    // longitude: number;

    @IsBoolean()
    @IsNotEmpty()
    @ApiProperty()
    @Transform(({ obj, key }) => {
    const value = obj[key];
    if (typeof value === 'string') {
      return obj[key] === 'true';
    }
        return value;
    })
    status: boolean;

}
