import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateSimulasiDto } from './dto/create-simulasi.dto';
import { UpdateSimulasiDto } from './dto/update-simulasi.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User, Role} from 'src/auth/entities';
import { UnitKerja } from 'src/karyawan/entities';
import { LocalFilesService } from 'src/local-files/local-files.service';
import { FindManyOptions, MoreThan, Repository } from 'typeorm';
import { Simulasi } from './entities/simulasi.entity';

@Injectable()
export class SimulasiService {

  constructor( 
    @InjectRepository(Simulasi)
    private simulasiRepository: Repository<Simulasi>,

    @InjectRepository(User)
    private userRepository: Repository<User>,

    @InjectRepository(Role)
    private roleRepository: Repository<Role>,

    @InjectRepository(UnitKerja)
    private unitKerjaRepository: Repository<UnitKerja>,

    private localFilesService: LocalFilesService


  ) {}
  
  async create(createSimulasiDto: CreateSimulasiDto, userId: number, fileData?: LocalFileDto) {
    try {
        const { respondent, report_date, report_time, environment_condition, weather_condition,
            cadets_condition, full_report, location, status } = createSimulasiDto;
        const cadets = await this.userRepository.findOneBy({ id: userId });
        const role = await this.roleRepository.findOneBy({ id: respondent });
        const isPending = await this.simulasiRepository.findOneBy({ status: false });

        if (isPending) {
            return {
              statusCode: HttpStatus.BAD_REQUEST,
              message: 'Terdapat Report yang belum diapprove'
            };
        } else {
            const newSimulasi: Simulasi = {
                respondent: role,
                cadets: cadets,
                report_date: report_date,
                report_time: report_time,
                environment_condition: environment_condition,
                weather_condition: weather_condition,
                cadets_condition: cadets_condition,
                full_report: full_report,
                location: location,
                // latitude: latitude,
                // longitude: longitude,
                status: status
            }

            const newData = await this.simulasiRepository.save(newSimulasi);

            if (fileData) {
                const responseFile = await this.localFilesService.saveLocalFileData(fileData);
                
                await this.simulasiRepository.update(newData.id, {
                  fileId: responseFile.id
                });
            }

            return newData;

        }
    } catch (e) {
      throw(e);
    }
  }

  async findAll(userId: number, status?: boolean, offset?: number, limit?:number, startId?: number) {
    const user = await this.userRepository.findOneBy({ id: userId });
    const roleId = user.role.id;
    const roleName = user.role.name;
    const role = await this.roleRepository.findOneBy({ id: roleId });
    
    const where: FindManyOptions<Simulasi>['where'] = {};
    
    if (roleName === "Taruna") {
      where.cadets = user;
    } else {
      where.respondent = role;
    }
    
    where.status = status;

    let separateCount = 0;
    if (startId) {
      where.id = MoreThan(startId);
      separateCount = await this.simulasiRepository.count();
    }

    const [items, count] = await this.simulasiRepository.findAndCount({
      where,
      order: {
        id: 'DESC'
      }, 
      skip: offset,
      take: limit
    })

    return {
      items, 
      count: startId ? separateCount : count,
    }
  }

  async findOne(id: number) {
    const simulasi = await this.simulasiRepository.findOne({
        where: { id },
    });
    if (simulasi) {
      return simulasi;
    }
  }

  async update(id: number, updateSimulasiDto: Partial<UpdateSimulasiDto>) {
    await this.simulasiRepository.update({ id }, updateSimulasiDto);
    
    const simulasi = await this.simulasiRepository.findOne({
      where: { id },
    });
    if (simulasi) {
      return simulasi;
    }
    throw new HttpException('Simulasi jaga tidak ditemukan', HttpStatus.NOT_FOUND);

  }

  async remove(id: number) {
    const simulasi = await this.simulasiRepository.delete(id);
    if (!simulasi.affected) {
      throw new HttpException('Simulasi report not found', HttpStatus.NOT_FOUND);
    }
    
    return {
      statusCode: HttpStatus.OK,
      message: 'Data successfully deleted!'
    };
  }

  async getStatus() {
    const isPending = await this.simulasiRepository.findOneBy({ status: false });
    if (isPending) {
        return {
            statusCode: HttpStatus.OK,
            message: false
        }
    } else {
        return {
            statusCode: HttpStatus.OK,
            message: true
        }
    }
  }

}
