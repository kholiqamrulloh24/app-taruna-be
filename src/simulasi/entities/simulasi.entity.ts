import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, 
    CreateDateColumn, UpdateDateColumn, OneToOne } from "typeorm";
import { User, Role } from "src/auth/entities";
import LocalFile from "src/local-files/entities/local-file.entity";
import { Transform } from "class-transformer";

@Entity()
export class Simulasi {
  @PrimaryGeneratedColumn()
  public id?: number;
   
  @Column()
  public report_date: string;

  @Column()
  public report_time: string;

  @Column()
  public environment_condition: string;

  @Column()
  public weather_condition: string;

  @Column()
  public cadets_condition: string;

  @Column()
  public full_report: string;

  @Column()
  public location: string;

  @ManyToOne(() => Role, (role) => role.id, { eager: true })
  @JoinColumn()
  public respondent: Role;

  @ManyToOne( () => User, (user) => user.id, {eager: true})
  @JoinColumn()
  cadets : User;

  @JoinColumn({ name: 'fileId' })
  @OneToOne(()=> LocalFile, { nullable: true })
  public file?: LocalFile;
  
  @Column({ nullable: true })
  public fileId?: number;

  @Column()
  public status: boolean;

  @CreateDateColumn()
  created_at?: Date;

  @UpdateDateColumn()
  updated_at?: Date;

}
