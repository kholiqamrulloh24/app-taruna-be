import { Test, TestingModule } from '@nestjs/testing';
import { CadetsResponseService } from './cadets-response.service';

describe('CadetsResponseService', () => {
  let service: CadetsResponseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CadetsResponseService],
    }).compile();

    service = module.get<CadetsResponseService>(CadetsResponseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
