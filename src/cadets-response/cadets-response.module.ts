import { Module } from '@nestjs/common';
import { CadetsResponseService } from './cadets-response.service';
import { CadetsResponseController } from './cadets-response.controller';
import { LocalFilesModule } from 'src/local-files/local-files.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CadetsResponse } from './entities/cadets-response.entity';
import { ConfigModule } from '@nestjs/config';
import { User, Role } from 'src/auth/entities';
import { UnitKerja } from 'src/karyawan/entities';

@Module({
  imports: [TypeOrmModule.forFeature([CadetsResponse, User, Role, UnitKerja]), LocalFilesModule, ConfigModule],
  controllers: [CadetsResponseController],
  providers: [CadetsResponseService]
})
export class CadetsResponseModule {}
