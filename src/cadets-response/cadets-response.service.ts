import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User, Role} from 'src/auth/entities';
import { UnitKerja } from 'src/karyawan/entities';
import { LocalFilesService } from 'src/local-files/local-files.service';
import { FindManyOptions, MoreThan, Repository } from 'typeorm';
import { CreateCadetsResponseDto } from './dto/create-cadets-response.dto';
import { UpdateCadetsResponseDto } from './dto/update-cadets-response.dto';
import { CadetsResponse } from './entities/cadets-response.entity';

@Injectable()
export class CadetsResponseService {

  constructor(
    @InjectRepository(CadetsResponse)
    private cadetsResponseRepository: Repository<CadetsResponse>,
    
    @InjectRepository(User)
    private userRepository: Repository<User>,

    @InjectRepository(Role)
    private roleRepository: Repository<Role>,

    @InjectRepository(UnitKerja)
    private unitKerjaRepository: Repository<UnitKerja>,

    private localFilesService: LocalFilesService
    
  ) {}
  
  async create(createCadetsResponseDto: CreateCadetsResponseDto, userId: number, fileData?: LocalFileDto) {
    try {
      const { condition, respondent } = createCadetsResponseDto;
      const cadets = await this.userRepository.findOneBy({ id: userId });
      //const perwira = await this.userRepository.findOneBy({ id: respondent });
      //const role = await this.roleRepository.findOneBy({ id: respondent });
      const unitKerja = await this.unitKerjaRepository.findOneBy({ id: respondent });
      const newCadetsResponse: CadetsResponse = {
        condition,
        respondent: unitKerja,
        cadets: cadets,
      }

      const newData = await this.cadetsResponseRepository.save(newCadetsResponse);
      
      if (fileData) {
        const responseFile = await this.localFilesService.saveLocalFileData(fileData);
        
        await this.cadetsResponseRepository.update(newData.id, {
          fileId: responseFile.id
        });
      }
      

      return newData;
    } catch (e) {
      throw(e);
    }

  }

  async uploadFile(userId: number, fileData: LocalFileDto) {
    const responseFile = await this.localFilesService.saveLocalFileData(fileData);
    await this.cadetsResponseRepository.update(userId, {
      fileId : responseFile.id
    });
  }

  async findAll(userId: number, offset?: number, limit?:number, startId?: number) {
    const user = await this.userRepository.findOneBy({ id: userId });
    const roleId = user.role.id;
    const roleName = user.role.name;
    const role = await this.roleRepository.findOneBy({ id: roleId });
    
    const where: FindManyOptions<CadetsResponse>['where'] = {};
    
    if (roleName === "Taruna") {
      where.cadets = user;
    }

    if (user.karyawanDetail) {
      const unitKerjaId = user.karyawanDetail.unitKerja.id;
      const unitKerja = await this.unitKerjaRepository.findOneBy({ id: unitKerjaId });
      where.respondent = unitKerja;
    }

    let separateCount = 0;
    if (startId) {
      where.id = MoreThan(startId);
      separateCount = await this.cadetsResponseRepository.count();
    }

    const [items, count] = await this.cadetsResponseRepository.findAndCount({
      where,
      order: {
        id: 'DESC'
      }, 
      skip: offset,
      take: limit
    })

    return {
      items, 
      count: startId ? separateCount : count,
    }
  }

  async findOne(id: number) {
    const cadetsReponse = await this.cadetsResponseRepository.findOne({
      where: { id },
    });

    if (cadetsReponse) {
      return cadetsReponse;
    }
  }

  async update(id: number, updateCadetsResponseDto: UpdateCadetsResponseDto) {
    await this.cadetsResponseRepository.update({ id }, updateCadetsResponseDto);
    
    const cadetsResponse = await this.cadetsResponseRepository.findOne({
      where: { id },
    });

    if (cadetsResponse) {
      return cadetsResponse;
    }
    
    throw new HttpException('Cadets response not found', HttpStatus.NOT_FOUND);

  }

  async remove(id: number) {
    const cadetsResponse = await this.cadetsResponseRepository.delete(id);
    if (!cadetsResponse.affected) {
      throw new HttpException('Cadets response not found', HttpStatus.NOT_FOUND);
    }
    
    return {
      statusCode: HttpStatus.OK,
      message: 'Data successfully deleted!'
    };

  }
}
