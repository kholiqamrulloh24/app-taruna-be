import { warn } from "console";
import { User, Role } from "src/auth/entities";
import { UnitKerja } from "src/karyawan/entities";
import LocalFile from "src/local-files/entities/local-file.entity";
import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToOne } from "typeorm";

@Entity()
export class CadetsResponse {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Column()
  public condition: string;

  @Column({ nullable: true })
  public path?: string;

  @JoinColumn({ name: 'fileId' })
  @OneToOne(()=> LocalFile, { nullable: true })
  public file?: LocalFile;
  
  @Column({ nullable: true })
  public fileId?: number;

  @JoinColumn()
  @ManyToOne( () => UnitKerja, (unitKerja) => unitKerja.id, { eager: true })
  public respondent: UnitKerja;

  @JoinColumn()
  @ManyToOne(() => User, (user) => user.id, { eager: true })
  public cadets: User;

  @CreateDateColumn()
  created_at?: Date;

  @UpdateDateColumn()
  updated_at?: Date;
}
