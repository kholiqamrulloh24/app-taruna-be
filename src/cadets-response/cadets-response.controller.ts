import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Req, UseInterceptors, BadRequestException, UploadedFile, Query } from '@nestjs/common';
import { CadetsResponseService } from './cadets-response.service';
import { CreateCadetsResponseDto } from './dto/create-cadets-response.dto';
import { UpdateCadetsResponseDto } from './dto/update-cadets-response.dto';
import { ApiConsumes, ApiCreatedResponse, ApiOkResponse, ApiTags, ApiBody } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth';
import { request } from 'http';
import LocalFilesInterceptor from 'src/local-files/local-files.interceptor';
import { Express } from 'express';
import LocalFile from 'src/local-files/entities/local-file.entity';
import { callbackify } from 'util';
import { PaginationParams } from 'src/utils/types/paginationParams';

@Controller('cadets-response')
@ApiTags('cadets-response')
export class CadetsResponseController {
  constructor(private readonly cadetsResponseService: CadetsResponseService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(LocalFilesInterceptor({
    fieldName: 'file',
    path: '/cadets-responses',
    fileFilter: (request, file, callback) => {
      if (!file.mimetype.includes('image')) {
         return callback(new BadRequestException('Provide a valid image'), false);
      }
      callback(null, true);
    },
    limits: {
      fileSize: Math.pow(1024, 2)
    }
  }))
  @ApiConsumes('multipart/form-data')
  create(@Req() request, @Body() createCadetsResponseDto: CreateCadetsResponseDto, @UploadedFile() file?: Express.Multer.File) {
    const userId = request.user.id;
    
    return this.cadetsResponseService.create(createCadetsResponseDto, userId, file);
  }
  
  @UseGuards(JwtAuthGuard)
  @Post('file')
  @UseInterceptors(LocalFilesInterceptor({
    fieldName: 'file',
    path: '/cadets-responses',
    fileFilter: (request, file, callback) => {
      if (!file.mimetype.includes('image')) {
        return callback(new BadRequestException('Provide a valid image'), false);
      }
      callback(null, true);
    },
    limits: {
      fileSize: Math.pow(1024, 2)
    }
  }))
  async uploadFile(@Req() request, @UploadedFile() file: Express.Multer.File) {
    return this.cadetsResponseService.uploadFile(1, {
      path: file.path,
      filename: file.originalname,
      mimetype: file.mimetype
    })
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@Req() request,
    @Query() { offset, limit, startId } : PaginationParams
  ) {
    const userId = request.user.id;
    return await this.cadetsResponseService.findAll(userId, offset, limit, startId);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.cadetsResponseService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCadetsResponseDto: UpdateCadetsResponseDto) {
    return this.cadetsResponseService.update(+id, updateCadetsResponseDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cadetsResponseService.remove(+id);
  }
}
