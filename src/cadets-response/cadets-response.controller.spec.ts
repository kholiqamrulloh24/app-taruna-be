import { Test, TestingModule } from '@nestjs/testing';
import { CadetsResponseController } from './cadets-response.controller';
import { CadetsResponseService } from './cadets-response.service';

describe('CadetsResponseController', () => {
  let controller: CadetsResponseController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CadetsResponseController],
      providers: [CadetsResponseService],
    }).compile();

    controller = module.get<CadetsResponseController>(CadetsResponseController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
