import { ApiProperty } from '@nestjs/swagger';
import { 
    IsNotEmpty,
    IsNotEmptyObject,
    IsNumber,
    IsString,
} from "class-validator";

export class UpdateCadetsResponseDto {
  
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  condition: string;

}
 
