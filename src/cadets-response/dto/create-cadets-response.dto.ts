import { ApiProperty } from "@nestjs/swagger";
import { 
    IsNotEmpty,
    IsNotEmptyObject,
    IsNumber,
    IsString,
} from "class-validator";
import { JoinColumn, ManyToOne } from "typeorm";
import { User } from "src/auth/entities";

export class CreateCadetsResponseDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    condition: string;
    
    @IsNotEmpty()
    @ApiProperty()
    respondent: number;
 
    //@IsNotEmpty()
    //@ApiProperty({ type: 'string', format: 'binary' })
    //file: Express.Multer.File;

}
