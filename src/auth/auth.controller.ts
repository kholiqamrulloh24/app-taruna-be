import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  InternalServerErrorException,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { LoginDto } from 'src/auth/dto/login-dto';
import { CadetsRegistrationDto } from 'src/auth/dto/cadets-registration-dto';
import { AuthService } from './auth.service';
import { KaryawanRegistrationDto } from './dto/karyawan-registration-dto';
import { ApiTags } from '@nestjs/swagger';
import { CreatePushNotifTokenDto } from './dto/create-push-notif-token.dto';
import { JwtAuthGuard } from './jwt.auth-guard';
import { Request } from 'express';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities';
import PushNotifToken from './entities/push-notif-token.entity';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(PushNotifToken)
    private pushNotifTokenRepository: Repository<PushNotifToken>,
  ) {}

  @Post('login')
  async login(@Body() loginDto: LoginDto) {
    const user = await this.authService.login(
      loginDto.username,
      loginDto.password,
    );

    if (user) {
      return user;
    } else {
      throw new HttpException('User not found', HttpStatus.FORBIDDEN);
    }
  }

  @Post('register')
  async register(@Body() registrationDto: CadetsRegistrationDto) {
    try {
      return await this.authService.registerAsCadets(registrationDto);
    } catch (e) {
      let errorMessage = 'Unknown';
      if (e instanceof Error) errorMessage = e.message;
      throw new InternalServerErrorException(errorMessage);
    }
  }

  @Post('register-karyawan')
  async registerAsKaryawan(@Body() registrationDto: KaryawanRegistrationDto) {
    try {
      return await this.authService.registerAsKaryawan(registrationDto);
    } catch (e) {
      if (e instanceof HttpException) {
        throw e;
      } else {
        let errorMessage = 'Unknown';
        if (e instanceof Error) errorMessage = e.message;
        throw new InternalServerErrorException(errorMessage);
      }
    }
  }

  @Get('role')
  async getAllRoles() {
    try {
      return await this.authService.allRoles();
    } catch (e) {
      if (e instanceof HttpException) {
        throw e;
      } else {
        let errorMessage = 'Unknown';
        if (e instanceof Error) errorMessage = e.message;
        throw new InternalServerErrorException(errorMessage);
      }
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('push-notif')
  async saveNewPushNotifToken(@Body() data: CreatePushNotifTokenDto, @Req() req) {
    const userId = req.user.id;
    const user: User = await this.userRepository.findOneBy({id: userId});

    const userTokens = user.pushNotifToken;

    let userHasThisToken = userTokens.find(userTokenItem => userTokenItem.token === data.token) !== undefined;
    
    if(!userHasThisToken) {
      const pushNotifToken = new PushNotifToken();
      pushNotifToken.token = data.token;
      pushNotifToken.user = user;
      
      this.pushNotifTokenRepository.save(pushNotifToken);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('logout')
  async logout(@Body() data: CreatePushNotifTokenDto, @Req() req) {
    const userId = req.user.id;
    const user: User = await this.userRepository.findOneBy({id: userId});

    const userTokens = user.pushNotifToken;

    let existingToken = userTokens.find(userTokenItem => userTokenItem.token === data.token);
    
    if(existingToken) {
      this.pushNotifTokenRepository.delete(existingToken);
    }
  }
}

