import { Test, TestingModule } from '@nestjs/testing';
import { PushNotifService } from './push-notif.service';

describe('PushNotifService', () => {
  let service: PushNotifService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PushNotifService],
    }).compile();

    service = module.get<PushNotifService>(PushNotifService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
