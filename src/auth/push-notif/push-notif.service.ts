import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';
import { Expo, ExpoPushErrorTicket, ExpoPushMessage, ExpoPushSuccessTicket, ExpoPushTicket } from 'expo-server-sdk';
import { LessThan, Repository, MoreThan, MoreThanOrEqual } from 'typeorm';
import PushNotifTicket from '../entities/need-to-check-push-notif-message.entity';
import PushNotifToken from '../entities/push-notif-token.entity';
import User from '../entities/user.entity';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class PushNotifService {

  private expo: Expo;

  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(PushNotifToken)
    private pushNotifTokenRepository: Repository<PushNotifToken>,
    @InjectRepository(PushNotifTicket)
    private pushNotifTicketRepository: Repository<PushNotifTicket>,
  ) {
    this.expo = new Expo();
  }

  async sendPushNotification(title: string, tokens: string[], message?: string) {
    console.log('send push notification', title, ' to ', tokens.length, 'recipients');
    let messages: ExpoPushMessage[] = [];
    for(let pushNotificationToken of tokens) {
      if(!Expo.isExpoPushToken(pushNotificationToken)) {
        continue;
      }

      messages.push({
        to: pushNotificationToken,
        sound: 'default',
        title: title,
        body: message
      });
    }

    let messageChunks = this.expo.chunkPushNotifications(messages);

    let tickets: ExpoPushTicket[] = [];
    for(let messageChunk of messageChunks) {
      try {
        let ticketChunk = await this.expo.sendPushNotificationsAsync(messageChunk);
        tickets = [
          ...tickets,
          ...ticketChunk
        ];
      } catch(e) {

      }
    }

    for(let ticketIndex in tickets) {
      let ticket = tickets[ticketIndex];
      let relatedMessage = messages[ticketIndex];
      if(ticket.status === 'ok') {
        let successTicket = ticket as ExpoPushSuccessTicket;
        this.pushNotifTicketRepository.save({
          title: relatedMessage.title,
          body: relatedMessage.body,
          numberOfAttempts: 1,
          lastAttemptAt: moment().unix(),
          receiptId: successTicket.id,
          token: Array.isArray(relatedMessage.to) ? relatedMessage.to.join(',') : relatedMessage.to,
        });
      } else {
        let errorTicket = ticket as ExpoPushErrorTicket;
        if(errorTicket.details.error === 'DeviceNotRegistered') {
          let invalidToken = tokens[ticketIndex];
          let invalidPushNotifToken = await this.pushNotifTokenRepository.findOneBy({token: invalidToken});
          if(invalidPushNotifToken) {
            this.pushNotifTokenRepository.delete(invalidPushNotifToken);
          }
        }
      }
    }


    // let receiptIdChunks = this.expo.chunkPushNotificationReceiptIds(receiptIds);
    // for(let receiptIdChunk of receiptIdChunks) {
    //   let receipts = await this.expo.getPushNotificationReceiptsAsync(receiptIdChunk);
    //   for(let receiptIndex in receipts) {
    //     const {
    //       status,
    //       details
    //     } = receipts[receiptIndex];
    //     if(status === 'ok') {
    //       continue;
    //     } else if(status === 'error') {
          
    //     }
    //   }
    // }
  }

  @Cron('0 */15 * * * *')
  async checkPushNotifTicketFirstTime() {
    let currentUnix = moment().unix();
    let a15minutesAgoUnix = currentUnix - (15*60);
    let savedTickets = await this.pushNotifTicketRepository.findBy({numberOfAttempts: 1, lastAttemptAt: LessThan(a15minutesAgoUnix)});
    console.log('cron setiap 15 menit', 'ada', savedTickets.length, 'ticket');

    let messages: ExpoPushMessage[] = [];

    let receiptIds: string[] = [];
    for(let ticket of savedTickets) {
      receiptIds.push(ticket.receiptId);
    }
    let receiptIdChunks = this.expo.chunkPushNotificationReceiptIds(receiptIds);
    for(let receiptIdChunk of receiptIdChunks) {
      let receipts = await this.expo.getPushNotificationReceiptsAsync(receiptIdChunk);
      for(let receiptIndex in receipts) {
        const {
          status,
          details,
        } = receipts[receiptIndex];
        if(status === 'ok') {
          let relatedTicket = await this.pushNotifTicketRepository.findOneBy({receiptId: receiptIndex});
          if(relatedTicket) {
            this.pushNotifTicketRepository.delete(relatedTicket);
          }
        } else if(status === 'error') {
          let savedTicket = savedTickets.find(item => item.receiptId === receiptIndex);
          if(details.error === 'DeviceNotRegistered') {
            if(savedTicket) {
              messages.push({
                to: savedTicket.token,
                title: savedTicket.title,
                sound: 'default',
                body: savedTicket.body
              });
            }
          }
        }
      }
    }

    let messageChunks = this.expo.chunkPushNotifications(messages);

    let tickets: ExpoPushTicket[] = [];
    for(let messageChunk of messageChunks) {
      try {
        let ticketChunk = await this.expo.sendPushNotificationsAsync(messageChunk);
        tickets = [
          ...tickets,
          ...ticketChunk
        ];
      } catch(e) {

      }
    }

    for(let ticketIndex in tickets) {
      let ticket = tickets[ticketIndex];
      let relatedSavedTicket = savedTickets[ticketIndex];
      if(ticket.status === 'ok') {
        this.pushNotifTicketRepository.delete(relatedSavedTicket);
      } else {
        let errorTicket = ticket as ExpoPushErrorTicket;
        if(errorTicket.details.error === 'DeviceNotRegistered') {
          let invalidToken = relatedSavedTicket.token;
          let invalidPushNotifToken = await this.pushNotifTokenRepository.findOneBy({token: invalidToken});
          if(invalidPushNotifToken) {
            this.pushNotifTokenRepository.delete(invalidPushNotifToken);
          }
        } else {
          relatedSavedTicket.numberOfAttempts += 1;
          relatedSavedTicket.lastAttemptAt = moment().unix();
        }
      }
    }
  }

  @Cron('0 */2 * * * *')
  async checkPushNotifTicketEvery2Minutes() {
    let currentUnix = moment().unix();
    let savedTickets = await this.pushNotifTicketRepository.findBy({numberOfAttempts: MoreThanOrEqual(2)});
    console.log('cron setiap 2 menit', 'ada', savedTickets.length, 'ticket');

    let messages: ExpoPushMessage[] = [];

    let receiptIds: string[] = [];
    for(let ticket of savedTickets) {
      let unixTresshold = currentUnix - (2*ticket.numberOfAttempts*60);
      if(ticket.lastAttemptAt <= unixTresshold) {
        receiptIds.push(ticket.receiptId);
      }
    }
    let receiptIdChunks = this.expo.chunkPushNotificationReceiptIds(receiptIds);
    for(let receiptIdChunk of receiptIdChunks) {
      let receipts = await this.expo.getPushNotificationReceiptsAsync(receiptIdChunk);
      for(let receiptIndex in receipts) {
        const {
          status,
          details,
        } = receipts[receiptIndex];
        if(status === 'ok') {
          continue;
        } else if(status === 'error') {
          let savedTicket = savedTickets.find(item => item.receiptId === receiptIndex);
          if(details.error === 'DeviceNotRegistered') {
            if(savedTicket) {
              messages.push({
                to: savedTicket.token,
                title: savedTicket.title,
                sound: 'default',
                body: savedTicket.body
              });
            }
          }
        }
      }
    }

    let messageChunks = this.expo.chunkPushNotifications(messages);

    let tickets: ExpoPushTicket[] = [];
    for(let messageChunk of messageChunks) {
      try {
        let ticketChunk = await this.expo.sendPushNotificationsAsync(messageChunk);
        tickets = [
          ...tickets,
          ...ticketChunk
        ];
      } catch(e) {

      }
    }

    for(let ticketIndex in tickets) {
      let ticket = tickets[ticketIndex];
      let relatedSavedTicket = savedTickets[ticketIndex];
      if(ticket.status === 'ok') {
        this.pushNotifTicketRepository.delete(relatedSavedTicket);
      } else {
        let errorTicket = ticket as ExpoPushErrorTicket;
        if(errorTicket.details.error === 'DeviceNotRegistered') {
          let invalidToken = relatedSavedTicket.token;
          let invalidPushNotifToken = await this.pushNotifTokenRepository.findOneBy({token: invalidToken});
          if(invalidPushNotifToken) {
            this.pushNotifTokenRepository.delete(invalidPushNotifToken);
          }
        } else {
          relatedSavedTicket.numberOfAttempts += 1;
          relatedSavedTicket.lastAttemptAt = moment().unix();
        }
      }
    }
  }
}
