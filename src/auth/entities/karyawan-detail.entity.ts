import { Jabatan, UnitKerja } from 'src/karyawan/entities';
import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity('karyawan_details')
export class KaryawanDetail {
  @PrimaryGeneratedColumn()
  id: number;
  @ManyToOne(() => UnitKerja, { eager: true })
  @JoinColumn()
  unitKerja: UnitKerja;
  @ManyToOne(() => Jabatan, { eager: true })
  @JoinColumn()
  jabatan: Jabatan;
}
