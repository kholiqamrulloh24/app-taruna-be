import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import User from "./user.entity";

@Entity('push_notif_tokens')
export default class PushNotifToken {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  token: string;

  @ManyToOne(() => User)
  user: User;
}