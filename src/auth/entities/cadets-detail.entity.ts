import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('cadet_details')
export class CadetsDetail {
  @PrimaryGeneratedColumn()
  id?: number;
  @Column()
  nipd: string;
  @Column()
  kelas: string;
}
