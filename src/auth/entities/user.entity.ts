import { Exclude } from 'class-transformer';
import { CadetsResponse } from 'src/cadets-response/entities/cadets-response.entity';
import { Emergency } from 'src/emergency/entities/emergency.entity';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { CadetsDetail } from './cadets-detail.entity';
import { KaryawanDetail } from './karyawan-detail.entity';
import PushNotifToken from './push-notif-token.entity';
import Role from './role.entity';

@Entity('users')
export default class User {
  @PrimaryGeneratedColumn()
  id?: number;
  
  @Column()
  name: string;
  
  @Column()
  @Index({ unique: true })
  username: string;
  
  @Column()
  password: string;
  
  @ManyToOne(() => Role, (role) => role.users, { eager: true })
  @JoinColumn()
  role: Role;
  
  @OneToOne(() => KaryawanDetail, { cascade: true, eager: true })
  @JoinColumn()
  karyawanDetail?: KaryawanDetail;
  
  @OneToOne(() => CadetsDetail, { cascade: true, eager: true })
  @JoinColumn()
  cadetsDetail?: CadetsDetail;

  @OneToMany(() => CadetsResponse, (cadetsResponse) => cadetsResponse.respondent)
  responses?: CadetsResponse[];

  @OneToMany(() => Emergency, (emergency) => emergency.cadets)
  emergency?: Emergency[];

  @OneToMany(() => PushNotifToken, (token) => token.user, {eager: true})
  pushNotifToken?: PushNotifToken[];
}
