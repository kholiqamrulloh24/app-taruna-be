import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export default class PushNotifTicket {
  @PrimaryGeneratedColumn()
  id?: number;
  @Column()
  token: string;
  @Column()
  title: string;
  @Column()
  body?: string;
  @Column()
  receiptId: string;
  @Column()
  lastAttemptAt: number;
  @Column()
  numberOfAttempts: number;
}