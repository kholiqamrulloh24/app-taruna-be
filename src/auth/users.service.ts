import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { CadetsRegistrationDto } from "src/auth/dto/cadets-registration-dto";
import { KaryawanRegistrationDto } from "src/auth/dto/karyawan-registration-dto";
import { Jabatan, UnitKerja } from "src/karyawan/entities";
import { Repository } from "typeorm";
import { CadetsDetail } from "./entities/cadets-detail.entity";
import { KaryawanDetail } from "./entities/karyawan-detail.entity";
import Role from "./entities/role.entity";
import User from "./entities/user.entity";

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Role)
    private roleRepository: Repository<Role>,
    @InjectRepository(Jabatan)
    private jabatanRepository: Repository<Jabatan>,
    @InjectRepository(UnitKerja)
    private unitKerjaRepository: Repository<UnitKerja>,
  ) {}

  findOne(username: string): Promise<User | null> {
    return this.userRepository.findOneBy({ username });
  }

  findAll(): Promise<User[]> {
    return this.userRepository.find({});
  }

  async createNewCadets(cadetsRegistrationDto: CadetsRegistrationDto) {
    try {
      const { kelas, name, nipd, password, username } = cadetsRegistrationDto;
      const newCadetsDetail: CadetsDetail = {
        kelas,
        nipd,
      };
      const cadetRole = await this.roleRepository.findOneBy({ id: 1 });
      const newUser: User = {
        name,
        password,
        role: cadetRole,
        username,
        cadetsDetail: newCadetsDetail,
      };
      return await this.userRepository.save(newUser, { reload: true });
    } catch (e) {
      throw e;
    }
  }

  async createNewKaryawan(karyawanRegistrationDto: KaryawanRegistrationDto) {
    try {
      const { jabatanId, name, password, unitKerjaId, username, roleId } =
        karyawanRegistrationDto;

      const jabatan: Jabatan | null = await this.jabatanRepository.findOneBy({
        id: jabatanId,
      });
      if (!jabatan) throw new BadRequestException("Jabatan not found");

      const unitKerja: UnitKerja | null =
        await this.unitKerjaRepository.findOneBy({
          id: unitKerjaId,
        });
      if (!unitKerja) throw new BadRequestException("Unit kerja not found");

      const role: Role | null = await this.roleRepository.findOneBy({
        id: roleId,
      });
      if (!role) throw new BadRequestException("Role not found");

      const karyawanDetail = new KaryawanDetail();
      karyawanDetail.jabatan = jabatan;
      karyawanDetail.unitKerja = unitKerja;

      const newUser = new User();
      newUser.name = name;
      newUser.password = password;
      newUser.role = role;
      newUser.username = username;
      newUser.karyawanDetail = karyawanDetail;

      return await this.userRepository.save(newUser, { reload: true });
    } catch (e) {
      throw e;
    }
  }
}
