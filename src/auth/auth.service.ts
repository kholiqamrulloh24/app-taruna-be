import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { CadetsRegistrationDto } from 'src/auth/dto/cadets-registration-dto';
import UserType from 'src/response-type/user.type';

import { Repository } from 'typeorm';
import { KaryawanRegistrationDto } from './dto/karyawan-registration-dto';
import { Role, User} from './entities';
import { UsersService } from './users.service';

export interface RefreshTokenPayload {
  jti: number,
  sub: number
}

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    @InjectRepository(Role)
    private roleRepo: Repository<Role>,

    @InjectRepository(User)
    private userRepository: Repository<User>
  ) {}

  async login(username: string, password: string): Promise<UserType | null> {
    const user = await this.usersService.findOne(username);
    if (user && user.password === password) {
      const { password, id, ...authenticatedUser } = user;
      return {
        ...authenticatedUser,
        id,
        accessToken: this.jwtService.sign({
          sub: id,
          name: authenticatedUser.name,
          email: authenticatedUser.username,
        }),
      };
    }
    return null;
  }

  async registerAsCadets(registrationDto: CadetsRegistrationDto) {
    try {
      const newCreatedUser = await this.usersService.createNewCadets(
        registrationDto,
      );
      const loginResult = this.login(
        newCreatedUser.username,
        newCreatedUser.password,
      );
      return loginResult;
    } catch (e) {
      throw e;
    }
  }

  async registerAsKaryawan(registrationDto: KaryawanRegistrationDto) {
    try {
      const newCreatedUser = await this.usersService.createNewKaryawan(
        registrationDto,
      );
      const loginResult = this.login(
        newCreatedUser.username,
        newCreatedUser.password,
      );
      return loginResult;
    } catch (e) {
      throw e;
    }
  }

  async allRoles() {
    try {
      return this.roleRepo.find();
    } catch (e) {
      throw e;
    }
  }

  async getUserFromRefreshTokenPayload(payload: RefreshTokenPayload){
    const subId = payload.sub;

    if(!subId) {
      throw new UnprocessableEntityException('token malformed')
    }

    return this.userRepository.findOneBy({ 'id': subId});
  }
}
