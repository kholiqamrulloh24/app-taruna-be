import { IsNotEmpty, IsEmail, IsNumber } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CadetsRegistrationDto {
  @IsNotEmpty()
  @ApiProperty()
  nipd: string;

  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @IsEmail()
  @IsNotEmpty()
  @ApiProperty()
  username: string;
  
  @ApiProperty()
  @IsNotEmpty()
  password: string;
  
  @ApiProperty()
  @IsNotEmpty()
  kelas: string;
}
