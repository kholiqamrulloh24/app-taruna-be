import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEmail, IsNumber, IsString } from 'class-validator';

export class KaryawanRegistrationDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @IsEmail()
  @IsNotEmpty()
  @ApiProperty()
  username: string;
  
  @ApiProperty()
  @IsNotEmpty()
  password: string;
  
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  unitKerjaId: number;
  
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  jabatanId: number;
  
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  roleId: number;
}
