import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { AuthService } from "./auth.service";
import { jwtConstants } from "./constants";
import { JwtStrategy } from "./jwt.strategy";
import { AuthController } from "./auth.controller";
import { UsersService } from "./users.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import User from "./entities/user.entity";
import Role from "./entities/role.entity";
import { KaryawanModule } from "src/karyawan/karyawan.module";
import { PushNotifService } from './push-notif/push-notif.service';
import PushNotifToken from "./entities/push-notif-token.entity";
import PushNotifTicket from "./entities/need-to-check-push-notif-message.entity";

@Module({
  providers: [AuthService, JwtStrategy, UsersService, PushNotifService],
  imports: [
    KaryawanModule,
    PassportModule,
    JwtModule.register(jwtConstants),
    TypeOrmModule.forFeature([User, Role, PushNotifToken, PushNotifTicket]),
  ],
  exports: [AuthService, PushNotifService],
  controllers: [AuthController],
})
export class AuthModule {}
