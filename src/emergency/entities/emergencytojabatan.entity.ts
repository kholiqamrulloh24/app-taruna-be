import { Jabatan } from "src/karyawan/entities";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Emergency } from "./emergency.entity";

@Entity()
export class EmergencyToJabatan {
  @PrimaryGeneratedColumn()
  public id?: number;

  @ManyToOne(() => Emergency)
  public emergency: Emergency;

  @ManyToOne(() => Jabatan)
  public jabatan: Jabatan;

  @Column({default: false})
  public isApproved?: boolean;
}