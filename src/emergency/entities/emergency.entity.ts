import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, 
    CreateDateColumn, UpdateDateColumn, Generated, OneToMany } from "typeorm";
import { User, Role } from "src/auth/entities";
import { Jabatan } from "src/karyawan/entities";
import { EmergencyToJabatan } from "./emergencytojabatan.entity";

@Entity()
export class Emergency {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Column()
  uuid: string;

  @Column()
  public condition: string;

  @Column()
  public location: string;

  @Column("double")
  public latitude: number;

  @Column("double")
  public longitude: number;
 
  // @ManyToOne(() => Jabatan, (jabatan) => jabatan.id, { eager: true })
  // @JoinColumn()
  // public respondent: Jabatan;

  @OneToMany(() => EmergencyToJabatan, (emergencyToJabatan) => emergencyToJabatan.emergency)
  public emergencyToJabatan?: EmergencyToJabatan;

  @Column()
  public status: boolean;

  @ManyToOne( () => User, (user) => user.id, {eager: true})
  @JoinColumn()
  cadets : User;

  @CreateDateColumn()
  created_at?: Date;

  @UpdateDateColumn()
  updated_at?: Date;

}

