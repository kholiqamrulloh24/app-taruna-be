import { ApiProperty } from '@nestjs/swagger';
import { 
    IsBoolean,
    IsNotEmpty,
    IsNumber,
    IsString,
} from "class-validator";

export class UpdateEmergencyDto {
  @IsString()
  @ApiProperty()
  condition: string;

  @IsString()
  @ApiProperty()
  location: string;

  @IsNumber()
  @ApiProperty()
  latitude: number;

  @IsNumber()
  @ApiProperty()
  longitude: number;

  @IsBoolean()
  @ApiProperty()
  status: boolean;
}
