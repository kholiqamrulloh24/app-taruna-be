import { ApiProperty } from "@nestjs/swagger";
import { 
    IsBoolean,
    IsNotEmpty,
    IsNumber,
    IsString,
} from "class-validator";
import { JoinColumn, ManyToOne } from "typeorm";
import { User } from "src/auth/entities";

export class CreateEmergencyDto {

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    condition: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    location: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    latitude: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiProperty()
    longitude: number;

    @IsBoolean()
    @IsNotEmpty()
    @ApiProperty()
    status: boolean;
}
