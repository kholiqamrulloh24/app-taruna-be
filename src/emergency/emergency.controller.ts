import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Query, Req, ParseIntPipe } from '@nestjs/common';
import { EmergencyService } from './emergency.service';
import { CreateEmergencyDto } from './dto/create-emergency.dto';
import { UpdateEmergencyDto } from './dto/update-emergency.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth';
import { PaginationParams } from 'src/utils/types/paginationParams';
import { request } from 'http';

@Controller('emergencies')
@ApiTags('emergencies')
export class EmergencyController {
  constructor(private readonly emergencyService: EmergencyService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Req() request, @Body() createEmergencyDto: CreateEmergencyDto) {
    const userId = request.user.id;

    return this.emergencyService.create(createEmergencyDto, userId);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/check')
  checking(@Req() request){
    const userId = request.user.id;
    
    return this.emergencyService.checking(userId);
  }
  
  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@Req() request, @Query() query: { status : boolean},
        @Query() { offset, limit, startId } : PaginationParams
  ) {
    const userId = request.user.id;

    return this.emergencyService.findAll(userId, query.status , offset, limit, startId);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Req() request, @Param('id') id: string) {
    return this.emergencyService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@Param('id') uuid: string, @Body() updateEmergencyDto: Partial<UpdateEmergencyDto>) {
    return this.emergencyService.update(uuid, updateEmergencyDto);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('approve/:id')
  approve(@Param('id') emId: number, @Req() request, @Body() updateEmergencyDto: Partial<UpdateEmergencyDto>) {
    const userId = request.user.id;
    return this.emergencyService.approve(emId, userId);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.emergencyService.remove(+id);
  }
}
