import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateEmergencyDto } from './dto/create-emergency.dto';
import { UpdateEmergencyDto } from './dto/update-emergency.dto';
import { Emergency } from './entities/emergency.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, MoreThan, Repository, RelationOptions } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { ExtractJwt } from 'passport-jwt';
import { User, Role } from 'src/auth/entities';
import { Jabatan } from 'src/karyawan/entities';
import { PushNotifService } from 'src/auth/push-notif/push-notif.service';
import { KaryawanDetail } from 'src/auth/entities/karyawan-detail.entity';
import { v4 as uuidv4 } from 'uuid';
import { EmergencyToJabatan } from './entities/emergencytojabatan.entity';

@Injectable()
export class EmergencyService {
  
  constructor(
    @InjectRepository(Emergency)
    private emergencyRepository: Repository<Emergency>,
    
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,

    @InjectRepository(Jabatan)
    private readonly jabatanRepository: Repository<Jabatan>,

    @InjectRepository(KaryawanDetail)
    private readonly karyawanDetailRepository: Repository<KaryawanDetail>,

    @InjectRepository(EmergencyToJabatan)
    private readonly emergencyToJabatanRepository: Repository<EmergencyToJabatan>,

    private readonly jwtService: JwtService,

    private pushNotifService: PushNotifService,
  ){}
    
  async create(createEmergencyDto: CreateEmergencyDto, userId: number) {
    try {
      const { condition, location, latitude, longitude, status } = createEmergencyDto;
      const topManagementJabatan = await this.jabatanRepository.findBy({isTopManagement: true});
      const cadets = await this.userRepository.findOneBy({ id: userId });
      //const perwira = await this.userRepository.findOneBy({ id: respondent });

      const uuid = uuidv4();

      let emergency: Emergency = {
        uuid,  
        condition,
        location,
        latitude,
        longitude,
        cadets : cadets,
        status
      };
      this.emergencyRepository.save(emergency);
      
      let karyawanDetails: KaryawanDetail[] = [];
      for(let jabatan of topManagementJabatan) {
        let karyawanDetail = await this.karyawanDetailRepository.findOneBy({jabatan});
        if(karyawanDetail) {
          karyawanDetails.push(karyawanDetail);
        }

        let emergencyToJabatan: EmergencyToJabatan = {
          emergency: emergency,
          jabatan: jabatan,
        };

        this.emergencyToJabatanRepository.save(emergencyToJabatan);
      }

      let allReceiverTokens: string[] = [];
      for(let karyawanDetail of karyawanDetails) {
        let user = await this.userRepository.findOneBy({karyawanDetail});
        if(user) {
          let thisUserTokens = user.pushNotifToken;
          for(let pushNotifToken of thisUserTokens) {
            allReceiverTokens = [
              ...allReceiverTokens,
              pushNotifToken.token
            ];
          }
        }
      }

      this.pushNotifService.sendPushNotification('New Emergency Report from Cadet', allReceiverTokens, condition)
      .then(() => console.log('beres kirim'))
      .catch(() => {});

      return true;
    } catch (error) {
      throw(error);
    }
  }

  async checking(id: number): Promise<User | null> {
    //return this.authService.getUserFromRefreshTokenPayload();
    return this.userRepository.findOneBy({ id: id });
  }

  paginate(items:Array<Emergency>, page:number = 1, perPage:number = 10) {
    const offset:number = perPage * (page - 1);
    const totalPages:number = Math.ceil(items.length / perPage);
    const paginatedItems = items.slice(offset, perPage * page);
    const nextPage = (totalPages > page) ? page + 1 : null;

    return {
        page: page,
        perPage: perPage,
        previousPage: page - 1 ? page - 1 : null,
        nextPage: nextPage,
        total: items.length,
        totalPages: totalPages,
        items: paginatedItems
    };
  }

  async findAll(userId: number, status?: boolean, offset?: number, limit?:number, startId?: number) {
    const user = await this.userRepository.findOneBy({ id: userId });
    const roleId = user.role.id;
    const roleName = user.role.name;
    const role = await this.roleRepository.findOneBy({ id: roleId });

    const where: FindManyOptions<Emergency>['where'] = {};
    let relations: FindManyOptions<Emergency>['relations'] = {
      emergencyToJabatan: {
        jabatan: true
      }
    };
    
    if (roleName === "Taruna") {
      where.cadets = user;
    } else {
      const jabatanId = user.karyawanDetail.jabatan.id;
      const jabatan = await this.jabatanRepository.findOneBy({ id: jabatanId });

      where.emergencyToJabatan = { jabatan };
    }

    where.status = status;
    
    let separateCount = 0;
    
    if (startId) {
      where.id = MoreThan(startId);
      separateCount = await this.emergencyRepository.count();
    }

    const [raw, count] = await this.emergencyRepository.findAndCount({
      relations,
      where,
      order: {
        id: 'DESC'
      }
      // skip: offset,
      // take: limit
    })

    const key = 'uuid';

    const data = [...new Map(raw.map(item =>
      [item[key], item])).values()];
    console.log(offset);
    console.log(limit);
    const items = this.paginate(data, offset, limit);
    console.log(items);
    return items;

    // return {
    //   items, 
    //   // count: startId ? separateCount : items.length
    // }
  }

  async findOne(id: number) {
    const emergency = await this.emergencyRepository.findOne({
            relations: {
              emergencyToJabatan: {
                jabatan: true
              }
            },
            where: { id },
        });
    if (emergency) {
            return emergency;
        }
  }

  async update(uuid: string, updateEmergencyDto: Partial<UpdateEmergencyDto>) {
    await this.emergencyRepository.update({ uuid }, updateEmergencyDto);
    
    const emergency = await this.emergencyRepository.find({
      where: { uuid },
    });

    if (emergency) {
      return emergency;
    }
    throw new HttpException('Emergency report not found', HttpStatus.NOT_FOUND);
 
  }

  async approve(emId: number, userId: number) {
    const emergency = await this.emergencyRepository.findOneBy({ id: emId });
    const user = await this.userRepository.findOneBy({ id: userId });

    if(!user.karyawanDetail) {
      throw new HttpException('Karyawan role is required', HttpStatus.NOT_FOUND);
    }

    const jabatan = user.karyawanDetail.jabatan;

    if(!emergency) {
      throw new HttpException('Emergency report not found', HttpStatus.NOT_FOUND);
    }

    console.log('ada');

    if(!jabatan) {
      throw new HttpException('Jabatan not found', HttpStatus.NOT_FOUND);
    }

    const emergencyToJabatan = await this.emergencyToJabatanRepository.findOne({
      relations: {
        emergency: true,
        jabatan: true
      },
      where: {
        emergency: {
          id: emergency.id
        },
        jabatan: {
          id: jabatan.id
        }
      }
    });
    console.log(emergencyToJabatan);

    if(!emergencyToJabatan) {
      throw new HttpException('Data not found', HttpStatus.NOT_FOUND);
    }

    emergencyToJabatan.isApproved = true;
    await this.emergencyToJabatanRepository.save(emergencyToJabatan);
    emergency.status = true;
    await this.emergencyRepository.save(emergency);
    return emergency;
  }

  async remove(id: number) {
    const emergency = await this.emergencyRepository.delete(id);
    if (!emergency.affected) {
      throw new HttpException('Emergency report not found', HttpStatus.NOT_FOUND);
    }
    
    return {
      statusCode: HttpStatus.OK,
      message: 'Data successfully deleted!'
    };

  }
}
