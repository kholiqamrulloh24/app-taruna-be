import { Module } from '@nestjs/common';
import { EmergencyService } from './emergency.service';
import { EmergencyController } from './emergency.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Emergency } from './entities/emergency.entity';
import { User, Role } from 'src/auth/entities';
import { JwtService } from '@nestjs/jwt';
import { AuthModule, AuthService } from 'src/auth';
import { UsersService } from 'src/auth/users.service';
import { Jabatan } from 'src/karyawan/entities';
import { KaryawanDetail } from 'src/auth/entities/karyawan-detail.entity';
import { EmergencyToJabatan } from './entities/emergencytojabatan.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Emergency, User, Role, Jabatan, KaryawanDetail, EmergencyToJabatan]), AuthModule],
  exports: [TypeOrmModule],
  controllers: [EmergencyController],
  providers: [EmergencyService, JwtService]
})

export class EmergencyModule {}
