import { IsNumber, Min, IsOptional, IsInt } from "class-validator";
import { Type, Transform } from "class-transformer";

export class PaginationParams {
  @IsOptional()
  @Type(() => Number )
  @IsNumber()
  @Min(1)
  startId?: number;

  @IsOptional()
  // @Transform((value) => Number(value))
  @Type(() => Number )
  @IsNumber()
  // @Min(0)
  offset?: number;

  @IsOptional()
  // @Transform((value) => Number(value))
  @Type(() => Number )
  @IsNumber()
  // @Min(0)
  limit?: number;


}
