import { Seeder, SeederFactoryManager } from "typeorm-extension";
import { DataSource } from "typeorm";
import Role from "../../auth/entities/role.entity";

export default class RoleSeeder implements Seeder {
    public async run(
        dataSource : DataSource,
        factoryManager : SeederFactoryManager
    ): Promise<any> {
        const repository = dataSource.getRepository(Role);
        await repository.insert([
            {
                name: "bimsuhtar"
            },
            {
                name: "perwira_jaga"
            }, 
            {
                name: "perwira_dinas"
            },
            {
                name: "perwira_pengawas"
            }
        ]);
    }
}
