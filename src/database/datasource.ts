import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { ConfigService } from "@nestjs/config";
import { CadetsDetail } from "src/auth/entities/cadets-detail.entity";
import { KaryawanDetail } from "src/auth/entities/karyawan-detail.entity";
import { Jabatan, UnitKerja } from "src/karyawan/entities";
import { Role, User } from "src/auth/entities";
import { Emergency } from "src/emergency/entities/emergency.entity";
import { CadetsResponse } from "src/cadets-response/entities/cadets-response.entity";
import LocalFile from "src/local-files/entities/local-file.entity";
import { Simulasi } from "src/simulasi/entities/simulasi.entity";
import PushNotifToken from "src/auth/entities/push-notif-token.entity";
import PushNotifTicket from "src/auth/entities/need-to-check-push-notif-message.entity";
import { EmergencyToJabatan } from "src/emergency/entities/emergencytojabatan.entity";

const AppDataSourceOptions = (
  configService: ConfigService,
): TypeOrmModuleOptions => ({
  type: "mysql",
  host: configService.get<string>("DB_HOST"),
  port: 3306,
  username: configService.get<string>("DB_USERNAME"),
  password: configService.get<string>("DB_PASSWORD"),
  database: configService.get<string>("DB_DATABASENAME"),
  entities: [
    User,
    Role,
    CadetsDetail,
    Jabatan,
    KaryawanDetail,
    UnitKerja,
    Emergency,
    CadetsResponse,
    LocalFile,
    Simulasi,
    PushNotifToken,
    PushNotifTicket,
    EmergencyToJabatan,
  ],
  synchronize: configService.get<boolean>("DB_ENTITY_SYNC") || false,
});

export default AppDataSourceOptions;
