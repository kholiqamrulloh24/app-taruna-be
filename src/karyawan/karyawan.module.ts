import { Module } from "@nestjs/common";
import { JabatanController } from "./jabatan/jabatan.controller";
import { UnitKerjaController } from "./unit-kerja/unit-kerja.controller";
import { JabatanService } from "./jabatan/jabatan.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Jabatan, UnitKerja } from "./entities";
import { UnitKerjaService } from "./unit-kerja/unit-kerja.service";

@Module({
  imports: [TypeOrmModule.forFeature([Jabatan, UnitKerja])],
  exports: [TypeOrmModule],
  controllers: [JabatanController, UnitKerjaController],
  providers: [JabatanService, UnitKerjaService],
})
export class KaryawanModule {}
