import { EmergencyToJabatan } from 'src/emergency/entities/emergencytojabatan.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('jabatan')
export class Jabatan {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({default: false})
  isTopManagement: boolean;

  @OneToMany(() => EmergencyToJabatan, (e) => e.jabatan)
  emergencyToJabatan?: EmergencyToJabatan;
}
