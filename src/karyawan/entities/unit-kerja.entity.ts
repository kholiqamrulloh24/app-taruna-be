import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('unit_kerja')
export class UnitKerja {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
}
