import { Controller, Get } from "@nestjs/common";
import { UnitKerjaService } from "./unit-kerja.service";

@Controller("unit-kerja")
export class UnitKerjaController {
  constructor(private unitKerjaService: UnitKerjaService) {}

  @Get()
  getAllUnitKerja() {
    return this.unitKerjaService.getAllUnitKerja();
  }
}
