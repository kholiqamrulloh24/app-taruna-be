import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UnitKerja } from "../entities";

@Injectable()
export class UnitKerjaService {
  constructor(
    @InjectRepository(UnitKerja)
    private unitKerjaRepo: Repository<UnitKerja>,
  ) {}

  async getAllUnitKerja() {
    return await this.unitKerjaRepo.find();
  }
}
