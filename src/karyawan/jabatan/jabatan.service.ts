import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Jabatan } from "../entities";

@Injectable()
export class JabatanService {
  constructor(
    @InjectRepository(Jabatan)
    private jabatanRepo: Repository<Jabatan>,
  ) {}

  async getAllJabatan() {
    return await this.jabatanRepo.find({ 
        // where: {
        //     isTopManagement: true, 
        // },
    });
  }
}
