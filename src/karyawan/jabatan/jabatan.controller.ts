import { Controller, Get } from '@nestjs/common';
import { JabatanService } from './jabatan.service';

@Controller('jabatan')
export class JabatanController {
  constructor(private jabatanService: JabatanService) {}

  @Get()
  getAllJabatan() {
    return this.jabatanService.getAllJabatan();
  }
}
