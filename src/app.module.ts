import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConfigModule, ConfigService } from "@nestjs/config";

import { AppService } from "./app.service";
import AppDataSourceOptions from "./database/datasource";
import { AuthModule } from "./auth";
import { KaryawanModule } from "./karyawan/karyawan.module";
import { EmergencyModule } from './emergency/emergency.module';
import { LocalFilesModule } from './local-files/local-files.module';
import { CadetsResponseModule } from './cadets-response/cadets-response.module';
import { SimulasiModule } from './simulasi/simulasi.module';
import { ScheduleModule } from "@nestjs/schedule";
import { HistoryModule } from './history/history.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: AppDataSourceOptions,
    }),
    ConfigModule.forRoot({
      envFilePath: `.env.${process.env.ENV}`,
    }),
    AuthModule,
    KaryawanModule,
    EmergencyModule,
    LocalFilesModule,
    CadetsResponseModule,
    SimulasiModule,
    ScheduleModule.forRoot(),
    HistoryModule
  ],
  providers: [AppService],
})
export class AppModule {}
